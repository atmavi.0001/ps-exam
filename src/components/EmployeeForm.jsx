import React from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';


const EmployeeForm = props => {
   const {
      first_name,
      last_name,
      address,
      birthdate,
      status,
      team
   } = props.employee;

   const emp = {
      first_name,
      last_name,
      address,
      birthdate,
      status,
      team
   }

   return (
      <Formik
         initialValues={emp}
         validationSchema={Yup.object({
            first_name: Yup.string()
               .required('Required'),
            last_name: Yup.string()
               .required('Required'),
            birthdate: Yup.string()
               .required('Required'),
            status: Yup.string()
               .required('Required'),
            team: Yup.string()
               .required('Required'),
         })}
         onSubmit={values => {
            console.log(props.onUpdate, values)

            if (props.onUpdate) {
               props.onUpdate(values)
            } else {
               props.handleNewEmployee(values)
            }
         }}
      >
         {formik => (
            <form onSubmit={formik.handleSubmit}>
               <label htmlFor="first_name">First Name</label>
               <input
                  id="first_name"
                  type="text"
                  {...formik.getFieldProps('first_name')}
               />
               {formik.touched.firstName && formik.errors.firstName ? (
                  <div>{formik.errors.firstName}</div>
               ) : null}

               <label htmlFor="last_name">Last Name</label>
               <input
                  id="last_name"
                  type="text"
                  {...formik.getFieldProps('last_name')}
               />
               {formik.touched.lastName && formik.errors.lastName ? (
                  <div>{formik.errors.lastName}</div>
               ) : null}

               <label htmlFor="address">Address</label>
               <input id="address" type="address" {...formik.getFieldProps('address')} />
               {formik.touched.address && formik.errors.address ? (
                  <div>{formik.errors.address}</div>
               ) : null}

               <label htmlFor="birthdate">Birthdate</label>
               <input id="birthdate" type="birthdate" {...formik.getFieldProps('birthdate')} />
               {formik.touched.birthdate && formik.errors.birthdate ? (
                  <div>{formik.errors.birthdate}</div>
               ) : null}

               <label htmlFor="team">Team</label>
               <input id="team" type="team" {...formik.getFieldProps('team')} />
               {formik.touched.team && formik.errors.team ? (
                  <div>{formik.errors.team}</div>
               ) : null}

               <label htmlFor="status">Status</label>
               <input id="status" type="status" {...formik.getFieldProps('status')} />
               {formik.touched.status && formik.errors.status ? (
                  <div>{formik.errors.status}</div>
               ) : null}

               <button
                  className="btn bg-green-400 mt-4"
                  type="submit">Submit</button>
            </form>
         )}
      </Formik>
   );
}

export default EmployeeForm;