import React from 'react';
import { Link } from 'react-router-dom';

const EmployeeCard = props => {
   const { first_name, last_name, team, status, _id } = props.employee;

   return (
      <div className={`employee-card ${status}`}>
         <Link to={`/employees/${_id}`}>
            <h3 className="uppercase">{`${last_name}, ${first_name}`}</h3>
         </Link>
         <span className="text-gray-600">{team}</span>
         <Link to={`/employees/${_id}`}>
            <button className="btn bg-green-200 mt-2 block">View</button>
         </Link>
      </div >
   );
}

export default EmployeeCard;