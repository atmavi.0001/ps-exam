import React, { useState, useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';

import { getEmployee, deleteEmployee } from '../services/employee';

const Employee = () => {

   const { id } = useParams();
   const [employee, setEmployee] = useState({});

   const history = useHistory();

   const deleteEmp = () => {
      deleteEmployee(id)
         .then(() => {
            alert('An employee record has been deleted');
            history.push('/');
         })
   };

   const editEmp = () => {
      history.push(`/employees/${id}/edit`);
   }

   useEffect(() => {
      getEmployee(id)
         .then(res => setEmployee(res))
         .catch(err => console.log(err));

   }, [id]);

   const { last_name, first_name, team, address, status } = employee;

   return (
      <div className="text-center">
         <h2 className="heading-secondary">Employee</h2>
         <h3>Name:
            <span className="emp-detail">{`${last_name}, ${first_name}`}</span>
         </h3>
         <div className={`badge badge--${team}`}>Team:
            <span className="emp-detail">{team}</span>
         </div>
         <div className="emp-address">Address:
            <span className="emp-detail">{address}</span>
         </div>
         <div className="emp-status">Status:
            <span className="emp-detail">{status}</span>
         </div>

         <button
            className="btn bg-yellow-600"
            onClick={editEmp}>Edit</button>
         <button
            className="btn bg-red-600 m-2"
            onClick={deleteEmp}>Delete</button>
      </div>
   );
}

export default Employee;