import React from 'react';
import Axios from 'axios';

import EmployeeForm from '../components/EmployeeForm';

const EmployeeNew = () => {
   const employee = { first_name: '', last_name: '', birthdate: '', address: '', team: '', status: '' };

   const config = {
      headers: {
         Authorization: "Bearer $2a$10$FA" + process.env.REACT_APP_TOKEN
      }
   }

   const handleNewEmployee = emp => {
      Axios.post(`https://api.exams.kloud.ai/employees/`, emp, config)
         .then(res => alert("Successfully added a new employee"))
         .catch(err => console.log(err))
   }

   return (
      <React.Fragment>
         <h2 className="heading-secondary">New Employee Form</h2>
         <EmployeeForm
            employee={employee}
            onUpdate={handleNewEmployee}
         />
      </React.Fragment>
   );
}

export default EmployeeNew;