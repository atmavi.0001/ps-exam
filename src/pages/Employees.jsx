import React, { useEffect, useState } from 'react';

import EmployeeCard from '../components/EmployeeCard';
import { getEmployees } from '../services/employee';

const Employees = () => {
   const [employees, setEmployees] = useState([]);

   useEffect(() => {
      getEmployees()
         .then(res => setEmployees(res))
   }, []);

   return (
      <React.Fragment>
         <h2 className="heading-secondary">Employees</h2>
         {employees.map(employee => (
            <EmployeeCard employee={employee} key={employee._id} />
         ))}
      </React.Fragment>
   );
}

export default Employees;