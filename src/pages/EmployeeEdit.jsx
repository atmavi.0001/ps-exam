import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import EmployeeForm from '../components/EmployeeForm';
import { getEmployee, updateEmployee } from '../services/employee'

const EmployeeEdit = () => {

   const { id } = useParams();
   const [employee, setEmployee] = useState({});
   const [isLoading, setIsLoading] = useState(true);

   const handleEmployeeUpdate = emp => {
      updateEmployee(id, emp)
         .then(() => {
            alert('Successfully updated an employee');
         })
   }

   useEffect(() => {
      getEmployee(id)
         .then(res => {
            setEmployee(res);
            setIsLoading(false);
         })
   }, [id]);

   if (isLoading) {
      return <h2>Loading...</h2>
   }

   return (
      <React.Fragment>
         <h2 className="heading-secondary">Edit Employee Form</h2>
         <EmployeeForm
            employee={employee}
            onUpdate={handleEmployeeUpdate}
         />
      </React.Fragment>
   );
}

export default EmployeeEdit;