import axios from 'axios';

const url = "https://api.exams.kloud.ai/";


//token's first few char gets trimmed, will solve this later 
const config = {
   headers: {
      Authorization: "Bearer $2a$10$FA" + process.env.REACT_APP_TOKEN
   }
}

export const getEmployees = () => {
   return axios.get(`${url}employees`, config)
      .then(res => res.data.data)
      .catch(err => console.log(err));
};

export const getEmployee = id => {
   return axios.get(`${url}employees/${id}`, config)
      .then(res => res.data.data)
      .catch(err => console.log(err));
}


export const deleteEmployee = id => {
   return axios.post(`${url}employees/${id}/delete`, {}, config)
      .then(res => res)
      .catch(err => console.log(err));
}

export const updateEmployee = (id, emp) => {
   return axios.put(`${url}employees/${id}`, emp, config)
      .then(res => res.data.data)
      .catch(err => console.log(err));
}
