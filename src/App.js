import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';

import Employees from './pages/Employees';
import Employee from './pages/Employee';
import EmployeeEdit from './pages/EmployeeEdit';
import EmployeeNew from './pages/EmployeeNew';

import './App.css'

function App() {
  return (
    <Router>
      <nav className="bg-gray-800 text-gray-100 flex justify-between items-center py-4 px-6">
        <div className="logo"><Link to="/">PS- Exam</Link></div>
        <ul className="flex">
          <li className="nav-link"><Link to="/">Dashboard</Link></li>
          <li className="nav-link"><Link to="/employees">Employees</Link></li>
          <li className="nav-link bg-green-600 rounded"><Link to="/employees/new">Add New Employee</Link></li>
        </ul>
      </nav>
      <main>
        <Switch>

          <Route path="/employees/new" exact>
            <EmployeeNew />
          </Route>

          <Route path="/employees/:id/edit" exact>
            <EmployeeEdit />
          </Route>

          <Route path="/employees/:id" exact>
            <Employee />
          </Route>

          <Route path="/about">
            <About />
          </Route>

          <Route path="/employees">
            <Employees />
          </Route>

          <Route path="/">
            <Dashboard />
          </Route>
        </Switch>
      </main>
    </Router>
  );
}

function About() {
  return <h2>About</h2>;
}


function Dashboard() {
  return <h2>Dashboard</h2>;
}

export default App;
